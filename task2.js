/*
Расширить предыдущие решние используя stream.Transform
Реализовать свой класс, который будет конвертировать результат crypto.createHash() (бинарные данные - хеш‑сумма) в hex формат.
Результат вывести в консоль и записать в файл.
Использовать pipe()
*/

let Transform = require("stream").Transform;
const fs = require('fs');
const crypto = require('crypto');


class MyBinToHex extends Transform {
  _transform(chunk, encoding, callback ) {
    this.push(crypto.createHash('md5').update(chunk).digest('hex'));
    callback();
  }
}

var readableStream = fs.createReadStream('task1.js');
var writableStream = fs.createWriteStream('md5-task2.txt');
var ob = new MyBinToHex();

readableStream.pipe(ob);
ob.pipe(process.stdout);
ob.pipe(writableStream);
