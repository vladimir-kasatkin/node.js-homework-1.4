/*
Создать два потока: чтение и запись файла.
Используя crypto.createHash() вычислить md5 читаемых данных.
Результат вывести в консоль и записать в файл.
Использовать pipe()
*/

const fs = require('fs');
const crypto = require('crypto');

var readableStream = fs.createReadStream('task1.js');
var oHash = crypto.createHash('md5');
var writableStream = fs.createWriteStream('md5-task1.txt');

// 2 раза читать файл - концептуально неправильно
//readableStream.pipe(oHash).pipe(process.stdout);
//readableStream.pipe(oHash).pipe(writableStream);

// так - работает. но если честно, для меня странно почему это работает :) ведь отдав поток в stdout, данных уже нет, чтобы отправить в запись файла, но интуитивно закомментированный вариант выше - тоже странный, не верю, что надо 2 раза считать файл...  как правильно ?
readableStream.pipe(oHash);
oHash.pipe(process.stdout);
oHash.pipe(writableStream);
