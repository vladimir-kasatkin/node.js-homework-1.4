/*
Реализовать свой класс на основе: Readable, Writable, Transform
Readable класс должен генерировать бесконечное кол-во случайных цифр.
Writable класс должен выводить полученные данные через _write в консоль.
Transform класс должен как-либо изменять данные и передавать их на дальнейшую обработку, но с интервалами в 1 сек.
Использовать pipe()
*/

let Transform = require("stream").Transform;
let Readable = require("stream").Readable;
let Writable = require("stream").Writable;

class MyReadable extends Readable {
  constructor() {
    super();
    this.i = 0;
  }

  _read(size) {
    setInterval(() => {
        this.i++;
        this.push(this.i.toString());
    }, 500);
  }
}

class MyTransform extends Transform {
  _transform(chunk, encoding, callback ) {
    setTimeout(() => {
        this.push(chunk + '+salt');
        callback();
    }, 1000);

  }
}

class MyWritable extends Writable {
  _write(chunk, encoding, callback) {
    console.log(chunk.toString());
    callback();
  }
}

var readableStream = new MyReadable();
var writableStream = new MyWritable();
var transformStream = new MyTransform();

readableStream.pipe(transformStream).pipe(writableStream);
